## 2180607587_luongthienhung



## Ho Ten: Lương Thiện Hưng

## MSSV: 2180607587

## Lớp: 21DTHD4
## Khoa: Công Nghệ Thông Tin
| Title              | Delete students from the system based on ID each student	                   |
|:-------------------:|:-----------------------------------------------|
| Value Statement     |   Delet student information from the system based on ID	|
| Acceptence Criter   | Acceptence Criterion 1: Students can be deleted from the system when ID is found
|                     |  Acceptence Criterion 2: Display a notification if the student has been successfully deleted from the system
| Definition Of Done  | If a student falls into 3 cases: Transfer school, Graduate, Expulsion<br>The teacher will delete all of that student's information. Based on ID to search|
|Test Case            |  TC_01: Check the student deletion function. By entering your student code <br> TC_02: Check whether the deletion was successful or not. by displaying notifications|
| Owner               | Thien Hung                       | 

